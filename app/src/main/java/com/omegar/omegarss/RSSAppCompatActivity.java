package com.omegar.omegarss;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.omegar.omegarss.Settings.Settings;

public class RSSAppCompatActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Settings.getInstance().initialize(this);
    }
}
