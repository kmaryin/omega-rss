package com.omegar.omegarss.RSS;

import android.support.annotation.Nullable;
import android.util.Log;

import com.omegar.omegarss.Settings.Settings;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Konstantin Maryin
 */
public class RSSItem {
    private static final String ELEMENT_TAG_ITEM = "item";
    private static final String ELEMENT_TAG_TITLE = "title";
    private static final String ELEMENT_TAG_PUBDATE = "pubDate";
    private static final String ELEMENT_TAG_LINK = "link";
    private static final String ELEMENT_TAG_DESCRIPTION = "description";
    private static final String ELEMENT_TAG_CREATOR = "dc:creator";
    private static final String ELEMENT_TAG_THUMBNAIL = "media:thumbnail";
    private static final String ELEMENT_TAG_CONTENT = "media:content";
    private static final String ELEMENT_TAG_ENCLOSURE = "enclosure";

    private static final String ELEMENT_ATTRIBUTE_URL = "url";
    private static final String ELEMENT_ATTRIBUTE_MEDIUM = "medium";
    private static final String ELEMENT_ATTRIBUTE_TYPE = "type";
    private static final String ELEMENT_ATTRIBUTE_VALUE_IMAGE = "image";

    /**
     * Title of item
     */
    private String title;
    /**
     * Publication date
     */
    private String pubDate;
    /**
     * Url address of rss item
     */
    private String link;
    /**
     * Description for rss item
     */
    private String description;
    /**
     * Creator of rss item
     */
    private String creator;
    /**
     * Thumbnail for item
     */
    private String thumbnailUrl;
    /**
     * Image of item
     */
    private String imageUrl;

    /**
     * Construct for new object RSSItem
     *
     * @param title title of adding item
     * @param pubDate publication date of adding item
     * @param link url link of adding item
     * @param description description of adding item
     * @param thumbnailUrl url of thumbnail image
     * @param imageUrl url of attached image
     */
    public RSSItem(final String title, final String pubDate, final String link,
                   final String description, @Nullable final String creator,
                   @Nullable final String thumbnailUrl, @Nullable final String imageUrl) {
        this.title = title;
        this.pubDate = pubDate;
        this.link = link;
        this.description = description;
        this.thumbnailUrl = thumbnailUrl;
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getLink() {
        return link;
    }

    public String getDescription() {
        return description;
    }

    public String getCreator() {
        return creator;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public void setPubDate(final String pubDate) {
        this.pubDate = pubDate;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public void setLink(final String link) {
        this.link = link;
    }

    public void setCreator(final String creator) {
        this.creator = creator;
    }

    public void setThumbnailUrl(final String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public void setImageUrl(final String url) {
        this.imageUrl = url;
    }

    @Override
    public String toString() {
        return String.format("RSS Item [%s]", this.title);
    }

    /**
     * Parsing and returns list of rss items from specified content
     *
     * @param content xml content of rss page
     * @return list of RSSItem object
     */
    public static List<RSSItem> getRSSItemsFromString(final String content) {
        List<RSSItem> items = new ArrayList<>();

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document document = db.parse(new ByteArrayInputStream(content.getBytes()));
            Element element = document.getDocumentElement();

            NodeList nodeList = element.getElementsByTagName(ELEMENT_TAG_ITEM);
            for (int index = 0; index < nodeList.getLength(); ++index) {
                Element entry = (Element) nodeList.item(index);
                Element titleElement = (Element) entry.
                        getElementsByTagName(ELEMENT_TAG_TITLE).item(0);
                Element descriptionElement = (Element) entry.
                        getElementsByTagName(ELEMENT_TAG_DESCRIPTION).item(0);
                Element linkElement = (Element) entry.
                        getElementsByTagName(ELEMENT_TAG_LINK).item(0);
                Element pubDateElement = (Element) entry.
                        getElementsByTagName(ELEMENT_TAG_PUBDATE).item(0);
                Element creatorElement = (Element) entry.
                        getElementsByTagName(ELEMENT_TAG_CREATOR).item(0);
                Element thumbnailElement = (Element) entry.
                        getElementsByTagName(ELEMENT_TAG_THUMBNAIL).item(0);
                Element enclosureElement = (Element) entry.
                        getElementsByTagName(ELEMENT_TAG_ENCLOSURE).item(0);

                NodeList mediaContentList = entry.getElementsByTagName(ELEMENT_TAG_CONTENT);
                String imageUrl = null;
                for (int contentIndex = 0; contentIndex < mediaContentList.getLength(); ++contentIndex) {
                    Element contentEntry = (Element) mediaContentList.item(contentIndex);
                    String mediumAttribute = contentEntry.getAttribute(ELEMENT_ATTRIBUTE_MEDIUM);
                    if (mediumAttribute != null &&
                            mediumAttribute.equals(ELEMENT_ATTRIBUTE_VALUE_IMAGE)) {
                        imageUrl = contentEntry.getAttribute(ELEMENT_ATTRIBUTE_URL);
                        break;
                    }
                }
                if (imageUrl == null && enclosureElement != null) {
                    String enclosureType = enclosureElement.getAttribute(ELEMENT_ATTRIBUTE_TYPE);
                    if (enclosureType.contains(ELEMENT_ATTRIBUTE_VALUE_IMAGE)) {
                        imageUrl = enclosureElement.getAttribute(ELEMENT_ATTRIBUTE_URL);
                    }
                }

                String creator = null;
                if (creatorElement != null) {
                    creator = creatorElement.getNodeValue();
                }

                String thumbnailUrl = null;
                if (thumbnailElement != null) {
                    thumbnailUrl = thumbnailElement.getAttribute(ELEMENT_ATTRIBUTE_URL);
                }

                items.add(new RSSItem(titleElement.getFirstChild().getNodeValue(),
                        pubDateElement.getFirstChild().getNodeValue(),
                        linkElement.getFirstChild().getNodeValue(),
                        descriptionElement.getFirstChild().getNodeValue(), creator,
                        thumbnailUrl, imageUrl));
            }

        } catch (ParserConfigurationException e) {
            Log.e(Settings.APPLICATION_LOG_TAG, "error while creating new document builder");
        } catch (SAXException|IOException e1) {
            Log.e(Settings.APPLICATION_LOG_TAG, "error while parsing content");
        }

        return items;
    }
}
