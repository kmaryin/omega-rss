package com.omegar.omegarss;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.TextHttpResponseHandler;
import com.omegar.omegarss.Connection.ContentLoader;
import com.omegar.omegarss.Connection.RSSDatabaseHelper;
import com.omegar.omegarss.ListAdapter.NewsFeedListAdapter;
import com.omegar.omegarss.RSS.RSSItem;
import com.omegar.omegarss.Settings.Settings;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public final class NewsFeedActivity extends RSSAppCompatActivity {
    /**
     * Intent extra for send information to settings activity
     */
    public static final String INTENT_EXTRA_LINK = "link";

    /**
     * Adapter for displaying rss items list
     */
    private NewsFeedListAdapter newsFeedListAdapter;
    /**
     * Database connector
     */
    private RSSDatabaseHelper dbConnector;

    /**
     * Progressbar loading view
     */
    private ProgressBar loading;
    /**
     * Items list view
     */
    private ListView itemsList;

    /**
     * Handler for main gui thread runner
     */
    private Handler uiHandler;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_feed);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loading = (ProgressBar) findViewById(R.id.news_feed_loading);
        itemsList = (ListView) findViewById(R.id.news_feed_items_list);
        newsFeedListAdapter = new NewsFeedListAdapter(this, null);
        itemsList.setAdapter(newsFeedListAdapter);
        itemsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                RSSItem item = (RSSItem) newsFeedListAdapter.getItem(i);
                Intent intent = new Intent(NewsFeedActivity.this, ShowRSSItemActivity.class);
                intent.putExtra(INTENT_EXTRA_LINK, item.getLink());
                startActivity(intent);
            }
        });

        final SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) findViewById(R.id.news_feed_refresh);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadContent();
                refreshLayout.setRefreshing(false);
            }
        });

        dbConnector = new RSSDatabaseHelper(this);

        uiHandler = new Handler();
    }

    @Override
    public void onResume() {
        super.onResume();

        reloadContent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(NewsFeedActivity.this, SettingsActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Reloading rss content
     */
    private void reloadContent() {
        loading.setVisibility(View.VISIBLE);
        itemsList.setVisibility(View.GONE);

        loadRSSItemsFromDB();
        loadRSSItemsFromServer();
    }

    /**
     * Loads rss content from database
     */
    private void loadRSSItemsFromDB() {
        new Thread(){
            @Override
            public void run() {
                final List<RSSItem> items = dbConnector.getAllRSSItems();

                uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        newsFeedListAdapter.addRSSItems(items);
                        showListAction();
                    }
                });
            }
        }.start();
    }

    /**
     * Loads rss content from external server
     */
    private void loadRSSItemsFromServer() {
        ContentLoader.getInstance().loadRSScontent(Settings.getInstance().getRSSUrl(),
            new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toast.makeText(NewsFeedActivity.this, getString(R.string.rss_content_loading_error),
                            Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    insertLoadedItems(RSSItem.getRSSItemsFromString(responseString));
                }
            });
    }

    /**
     * Save loaded rss items to database
     * @param items list of loaded RSSItem objects
     */
    private void insertLoadedItems(final List<RSSItem> items) {
        new Thread(){
            @Override
            public void run() {
                final List<RSSItem> insertedItems = new ArrayList<>();
                //insert reversed list to have last -> first structure of records in base
                for (int i = items.size() - 1; i >= 0; --i) {
                    if (dbConnector.insertRSSItem(items.get(i))) {
                        insertedItems.add(items.get(i));
                    }
                }

                uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        newsFeedListAdapter.addRSSItems(insertedItems);
                        showListAction();
                    }
                });
            }
        }.start();
    }

    /**
     * Hides loading progressbar and shows list items
     */
    private void showListAction() {
        loading.setVisibility(View.GONE);
        itemsList.setVisibility(View.VISIBLE);
    }
}
