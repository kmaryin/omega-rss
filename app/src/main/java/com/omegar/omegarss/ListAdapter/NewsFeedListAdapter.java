package com.omegar.omegarss.ListAdapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.omegar.omegarss.R;
import com.omegar.omegarss.RSS.RSSItem;
import com.omegar.omegarss.Utils.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Adapter for News feed on NewsFeedActivity
 * Each list item displayed as material design line with rounded image as avatar
 *
 * Created by Konstantin Maryin
 */
public class NewsFeedListAdapter extends BaseAdapter {
    /**
     * Length of displayed description text content characters. If description text length
     * surpass this value, it cut off to this value
     */
    private static final Integer DESCRIPTION_TEXT_LENGTH = 70;

    /**
     * Inflater for create android view from resource xml
     */
    private LayoutInflater inflater;
    /**
     * Displayed rss items
     */
    private List<RSSItem> items;
    /**
     * Bitmap for display if item not have thumbnail image
     */
    private Bitmap emptyThumbnail;

    /**
     * Construct
     *
     * @param context android activity context
     * @param itemsList list of items for displaying. May be null
     */
    public NewsFeedListAdapter(final Context context, @Nullable final List<RSSItem> itemsList) {
        inflater = LayoutInflater.from(context);
        items = new ArrayList<>();
        if (itemsList != null) {
            items.addAll(itemsList);
        }

        emptyThumbnail = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.rssicon);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(final int i) {
        return items.get(getCount() - i - 1);
    }

    @Override
    public long getItemId(final int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, final ViewGroup viewGroup) {
        final ViewHolder holder;
        final RSSItem item = (RSSItem) getItem(i);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.material_list_item, viewGroup, false);
            holder.title = (TextView)convertView.findViewById(R.id.material_list_item_title);
            holder.description = (TextView)convertView.findViewById(R.id.material_list_item_description);
            holder.image = (RoundedImageView) convertView.findViewById(R.id.material_list_item_image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(item.getTitle());
        if (item.getDescription().length() > DESCRIPTION_TEXT_LENGTH) {
            holder.description.setText(String.format("%s...", item.getDescription().
                    substring(0, DESCRIPTION_TEXT_LENGTH - 1)));
        } else {
            holder.description.setText(item.getDescription());
        }

        if (item.getThumbnailUrl() != null) {
            holder.image.setImageResource(android.R.color.transparent);
            Picasso.with(inflater.getContext()).load(item.getThumbnailUrl()).resize(50, 50)
                    .centerCrop().into(holder.image);
        } else {
            holder.image.setImageBitmap(emptyThumbnail);
        }
        return convertView;
    }

    /**
     * Add rss item to list
     * @param item RSSItem object
     */
    public void addRSSItem(RSSItem item) {
        items.add(item);
        notifyDataSetChanged();
    }

    /**
     * Add list of rss items
     * @param items list of RSSItem object
     */
    public void addRSSItems(List<RSSItem> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    /**
     * Holder for list item row. Initializes once and calls on each displaying of items
     */
    private class ViewHolder {
        RoundedImageView image;
        TextView title;
        TextView description;
    }
}
