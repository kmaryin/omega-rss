package com.omegar.omegarss.Settings;

import android.content.Context;
import android.content.SharedPreferences;

import com.omegar.omegarss.Connection.RSSDatabaseHelper;

/**
 * Class for store and save application settings
 * uses pattern singleton
 *
 * Created by Konstantin Maryin
 */
public final class Settings {
    /**
     * Tag for logging application action messages
     */
    public static final String APPLICATION_LOG_TAG = "Omega-R RSS feed";
    /**
     * Name of application database file
     */
    public static final String DATABASE_NAME = "omegarssfeed.db";
    /**
     * Current version of application database
     */
    public static final Integer DATABASE_VERSION = 1;

    /**
     * Key for store current rss url content
     */
    private static final String RSSURL_PREFERENCES_KEY = "rssurl";
    /**
     * Default application url address
     */
    private static final String RSSURL_PREFERENCES_DEFAULT_VALUE = "http://feeds.nbcnews.com/feeds/worldnews";

    /**
     * Application context for case data
     */
    private Context mContext;

    /**
     * Current rss url address
     */
    private String RSSurl;

    /**
     * instance of once created class
     */
    private static volatile Settings instance;

    /**
     * Returns instance of existed copy
     *
     * @return ContentLoader instance
     */
    public static Settings getInstance() {
        Settings localInstance = instance;
        if (localInstance == null) {
            synchronized (Settings.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Settings();
                }
            }
        }
        return localInstance;
    }

    /**
     * Construct
     * Has private state for realizing singleton pattern
     */
    private Settings() {
    }

    /**
     * Initialize class instance
     * @param context application context
     */
    public void initialize(Context context) {
        mContext = context;

        SharedPreferences preferences = mContext.getSharedPreferences("settings", Context.MODE_PRIVATE);
        RSSurl = preferences.getString(RSSURL_PREFERENCES_KEY, RSSURL_PREFERENCES_DEFAULT_VALUE);
    }

    /**
     * Setter of application rss url address
     * @param value url address
     */
    public void setRSSurl(String value) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences("settings", Context.MODE_PRIVATE).edit();
        editor.putString(RSSURL_PREFERENCES_KEY, value).apply();
        RSSurl = value;

        RSSDatabaseHelper dbHelper = new RSSDatabaseHelper(mContext);
        dbHelper.clearAllItems();
    }

    /**
     * Getter of application rss url address
     *
     * @return String url address
     */
    public String getRSSUrl() {
        return RSSurl;
    }
}
