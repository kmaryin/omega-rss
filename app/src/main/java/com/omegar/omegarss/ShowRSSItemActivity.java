
package com.omegar.omegarss;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.omegar.omegarss.Connection.RSSDatabaseHelper;
import com.omegar.omegarss.RSS.RSSItem;
import com.omegar.omegarss.Settings.Settings;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ShowRSSItemActivity extends AppCompatActivity {
    /**
     * Activity toolbar
     */
    private Toolbar toolbar;
    /**
     * RSSItem url
     */
    private String url;
    /**
     * WebView for displaying full rssitem content
     */
    private WebView webView;
    /**
     * Trigger for WebView displaying
     */
    private Boolean isWebViewShowed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_rssitem);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setToolbarNavigationBack();

        Bundle bundle = getIntent().getExtras();
        url = bundle.getString(NewsFeedActivity.INTENT_EXTRA_LINK);
        if (url == null) {
            finish();
        }

        RSSDatabaseHelper db = new RSSDatabaseHelper(this);
        RSSItem item = db.getRSSItemByLink(url);
        TextView title = (TextView) findViewById(R.id.show_rssitem_title);
        ImageView image = (ImageView) findViewById(R.id.show_rssitem_image);
        TextView pubDate = (TextView) findViewById(R.id.show_rssitem_pubdate);
        TextView description = (TextView) findViewById(R.id.show_rssitem_description);
        TextView author = (TextView) findViewById(R.id.show_rssitem_author);
        webView = (WebView) findViewById(R.id.show_rssitem_webview);
        webView.setVisibility(View.GONE);

        title.setText(item.getTitle());
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWebView();
            }
        });

        if (item.getImageUrl() != null) {
            Picasso.with(this).load(item.getImageUrl()).into(image);
        }

        SimpleDateFormat format = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z", Locale.US);
        Date date = null;
        try {
            date = format.parse(item.getPubDate());
        } catch (ParseException e) {
            Log.e(Settings.APPLICATION_LOG_TAG, "Error while parsing publication date");
        }
        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(ShowRSSItemActivity.this);
        String dateString;
        if (date != null) {
            dateString = dateFormat.format(date);
        } else {
            dateString = item.getPubDate();
        }
        pubDate.setText(String.format("%s: %s", getString(R.string.pub_date), dateString));

        description.setText(item.getDescription());
        if (item.getCreator() != null){
            author.setText(String.format("%s: %s", getString(R.string.creator), item.getCreator()));
        }

        isWebViewShowed = false;
    }

    /**
     * Show toolbar with left arrow action
     */
    private void setToolbarNavigationBack() {
        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    /**
     * Show toolbar with cross action
     */
    private void setToolbarNaNavigationClose() {
        toolbar.setNavigationIcon(R.drawable.close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideWebView();
            }
        });
    }

    /**
     * Show WebView content
     */
    private void showWebView() {
        webView.setVisibility(View.VISIBLE);
        webView.loadUrl(url);

        isWebViewShowed = true;

        setToolbarNaNavigationClose();
    }

    /**
     * Hiding Web view content
     */
    private void hideWebView() {
        webView.setVisibility(View.GONE);
        isWebViewShowed = false;
        setToolbarNavigationBack();
    }

    @Override
    public void onBackPressed() {
        if (isWebViewShowed) {
            hideWebView();
        }
    }
}
