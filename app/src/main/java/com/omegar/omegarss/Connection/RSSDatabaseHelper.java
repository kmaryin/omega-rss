package com.omegar.omegarss.Connection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.omegar.omegarss.RSS.RSSItem;
import com.omegar.omegarss.Settings.Settings;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for work with database connection
 *
 * Created by Konstantin Maryin
 */
public class RSSDatabaseHelper extends SQLiteOpenHelper {
    public static final String TABLE_ITEM = "rssitem";

    public static final String COLUMN_ITEM_TITLE = "title";
    public static final String COLUMN_ITEM_PUBDATE = "pubdate";
    public static final String COLUMN_ITEM_LINK = "link";
    public static final String COLUMN_ITEM_DESCRIPTION = "description";
    public static final String COLUMN_ITEM_CREATOR = "creator";
    public static final String COLUMN_ITEM_IMAGEURL = "imageurl";
    public static final String COLUMN_ITEM_THUMBNAILURL = "thumbnailurl";

    /**
     * Construct
     *
     * @param context android activity context
     */
    public RSSDatabaseHelper(final Context context) {
        super(context, Settings.DATABASE_NAME, null, Settings.DATABASE_VERSION);
    }

    /**
     * Construct
     *
     * @param context android activity context
     * @param name database filename
     * @param factory database cursor factory
     * @param version version of specified database
     */
    public RSSDatabaseHelper(final Context context, final String name,
                             final SQLiteDatabase.CursorFactory factory, final int version) {
        super(context, name, factory, version);
    }

    /**
     * Construct
     *
     * @param context android activity context
     * @param name database filename
     * @param factory database cursor factory
     * @param version version of specified database
     * @param errorHandler call-back class for database error action
     */
    public RSSDatabaseHelper(final Context context, final String name,
                             final SQLiteDatabase.CursorFactory factory, final int version,
                             final DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + TABLE_ITEM +
                " (" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_ITEM_TITLE + " TEXT NOT NULL, " +
                COLUMN_ITEM_DESCRIPTION + " TEXT NOT NULL, " +
                COLUMN_ITEM_PUBDATE + " TEXT NOT NULL, " +
                COLUMN_ITEM_LINK + " TEXT NOT NULL, " +
                COLUMN_ITEM_CREATOR + " TEXT, " +
                COLUMN_ITEM_IMAGEURL + " TEXT, " +
                COLUMN_ITEM_THUMBNAILURL + " TEXT);");
    }

    @Override
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int i, final int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF IT EXISTS " + TABLE_ITEM);
        sqLiteDatabase.execSQL("DROP TABLE IF IT EXISTS " + TABLE_ITEM);
        onCreate(sqLiteDatabase);
    }

    /**
     * Adjust database connection and returns list of all available rss items
     *
     * @return list of RSSItem object
     */
    public List<RSSItem> getAllRSSItems() {
        List<RSSItem> items = new ArrayList<>();

        Cursor cursor = getWritableDatabase().query(TABLE_ITEM, new String[]{COLUMN_ITEM_TITLE,
                COLUMN_ITEM_DESCRIPTION, COLUMN_ITEM_LINK, COLUMN_ITEM_PUBDATE, COLUMN_ITEM_CREATOR,
                COLUMN_ITEM_IMAGEURL, COLUMN_ITEM_THUMBNAILURL}, null, null, null, null, null);
        cursor.moveToFirst();

        for (int i = 0; i < cursor.getCount(); ++i) {
            RSSItem item = new RSSItem(cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_TITLE)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_PUBDATE)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_LINK)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_DESCRIPTION)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_CREATOR)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_THUMBNAILURL)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_IMAGEURL)));
            items.add(item);
            cursor.move(1);
        }

        cursor.close();

        return items;
    }

    /**
     * Adjust database connection and returns rss item by specified url address
     *
     * @param link url address of rss item
     * @return RSSItem object
     */
    public RSSItem getRSSItemByLink(final String link) {
        Cursor cursor = getWritableDatabase().query(TABLE_ITEM, new String[]{COLUMN_ITEM_TITLE,
                COLUMN_ITEM_DESCRIPTION, COLUMN_ITEM_LINK, COLUMN_ITEM_PUBDATE, COLUMN_ITEM_CREATOR,
                COLUMN_ITEM_IMAGEURL, COLUMN_ITEM_THUMBNAILURL}, String.format("%s = \"%s\"",
                COLUMN_ITEM_LINK, link), null, null, null, null);
        cursor.moveToFirst();

        if (cursor.getCount() == 0) {
            return null;
        }

        RSSItem item = new RSSItem(cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_TITLE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_PUBDATE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_LINK)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_DESCRIPTION)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_CREATOR)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_THUMBNAILURL)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_IMAGEURL)));
        cursor.close();

        return item;
    }

    /**
     * Adjust database connection and insert specified item to database
     *
     * @param item RSSItem object
     * @return boolean state of insert object
     */
    public boolean insertRSSItem(final RSSItem item) {
        if (getRSSItemByLink(item.getLink()) != null) {
            return false;
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_ITEM_TITLE, item.getTitle());
        contentValues.put(COLUMN_ITEM_PUBDATE, item.getPubDate());
        contentValues.put(COLUMN_ITEM_LINK, item.getLink());
        contentValues.put(COLUMN_ITEM_DESCRIPTION, item.getDescription());
        contentValues.put(COLUMN_ITEM_THUMBNAILURL, item.getThumbnailUrl());
        contentValues.put(COLUMN_ITEM_IMAGEURL, item.getImageUrl());

        getWritableDatabase().insert(TABLE_ITEM, null, contentValues);
        return true;
    }

    /**
     * Adjust database connection and delete all of rss items
     */
    public void clearAllItems() {
        getWritableDatabase().delete(TABLE_ITEM, null, null);
    }
}
