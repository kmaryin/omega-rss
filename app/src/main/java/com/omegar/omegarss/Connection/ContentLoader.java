package com.omegar.omegarss.Connection;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.ResponseHandlerInterface;

/**
 * Created by Konstantin Maryin
 *
 * Class for work with internet connection.
 * Uses pattern singleton
 */
public final class ContentLoader {
    /**
     * Timeout of async request
     */
    private static final int REQUEST_TIMEOUT = 30*1000;
    /**
     * Client for loading content asyncronically
     */
    private AsyncHttpClient client;

    /**
     * Instance of once created class
     */
    private static volatile ContentLoader instance;

    /**
     * Returns instance of existed copy
     *
     * @return ContentLoader instance
     */
    public static ContentLoader getInstance() {
        ContentLoader localInstance = instance;
        if (localInstance == null) {
            synchronized (ContentLoader.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ContentLoader();
                }
            }
        }
        return localInstance;
    }

    /**
     * Construct
     * Has private state for realizing singleton pattern
     */
    private ContentLoader() {
        client = new AsyncHttpClient();
        initializeClient();
    }

    /**
     * Initializing of AsyncHttpClient connection
     */
    private void initializeClient() {
        client.setTimeout(REQUEST_TIMEOUT);
        client.setEnableRedirects(true);
    }

    /**
     * Loads page content from specified url
     *
     * @param url url address of required content
     * @param handler call-back class for AsyncHttpConnection object
     */
    public void loadRSScontent(String url, ResponseHandlerInterface handler) {
        client.get(url, handler);
    }
}
