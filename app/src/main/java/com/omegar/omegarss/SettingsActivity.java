package com.omegar.omegarss;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.TextHttpResponseHandler;
import com.omegar.omegarss.Connection.ContentLoader;
import com.omegar.omegarss.RSS.RSSItem;
import com.omegar.omegarss.Settings.Settings;

import cz.msebera.android.httpclient.Header;

public class SettingsActivity extends AppCompatActivity {
    /**
     * View for displaying action
     */
    private View actionSaveView;
    /**
     * View of success action result
     */
    private View actionSaveSuccessView;
    /**
     * View of error acrion result
     */
    private View actionSaveErrorView;

    /**
     * EditText view for url address text
     */
    private EditText urlTextEdit;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, NewsFeedActivity.class);
                startActivity(intent);
                finish();
            }
        });

        actionSaveView = findViewById(R.id.settings_action_save_view);
        actionSaveSuccessView = findViewById(R.id.settings_action_save_success_view);
        actionSaveErrorView = findViewById(R.id.settings_action_save_error_view);

        actionSaveView.setVisibility(View.GONE);
        actionSaveSuccessView.setVisibility(View.GONE);
        actionSaveErrorView.setVisibility(View.GONE);

        urlTextEdit = (EditText) findViewById(R.id.settings_url_text);
        urlTextEdit.setText(Settings.getInstance().getRSSUrl());
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SettingsActivity.this, NewsFeedActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Action to save new url address
     * @param view specified view
     */
    public void onSaveButtonClick(View view) {
        showActionSaving();
        String url = urlTextEdit.getText().toString();
        try {
            ContentLoader.getInstance().loadRSScontent(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {
                    Toast.makeText(SettingsActivity.this, getString(R.string.settings_content_loading_error),
                            Toast.LENGTH_SHORT).show();
                    showActionSaveError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (RSSItem.getRSSItemsFromString(responseString).size() > 0) {
                        saveNewUrl();
                    } else {
                        showActionSaveError();
                    }
                }
            });
        } catch (Exception e) {
            showActionSaveError();
        }
    }

    /**
     * Saves new url address
     */
    private void saveNewUrl() {
        String url = urlTextEdit.getText().toString();
        Settings.getInstance().setRSSurl(url);
        showActionSaveSuccess();
    }

    /**
     * Show action save
     */
    private void showActionSaving() {
        actionSaveView.setVisibility(View.VISIBLE);
        actionSaveSuccessView.setVisibility(View.GONE);
        actionSaveErrorView.setVisibility(View.GONE);
    }

    /**
     * Show action save success
     */
    private void showActionSaveSuccess() {
        actionSaveView.setVisibility(View.GONE);
        actionSaveSuccessView.setVisibility(View.VISIBLE);
        actionSaveErrorView.setVisibility(View.GONE);
    }

    /**
     * Show action save error
     */
    private void showActionSaveError() {
        actionSaveView.setVisibility(View.GONE);
        actionSaveSuccessView.setVisibility(View.GONE);
        actionSaveErrorView.setVisibility(View.VISIBLE);
    }
}
